<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <script text="text/javascript" src="js/jquery.js"></script>
    <script text="text/javascript" src="js/main.js"></script>
    <script text="text/javascript" src="js/bootstrap.bundle.js"></script>
    <script text="text/javascript" src="js/all.js"></script>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.css">

    <title>Inventory Management System</title>
</head>

<body>

    <?php

    include_once "templates/header.php";

    ?>

    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card mx-auto">
                    <img src="images/user.png" style="width: 60%;" class="card-img-top mx-auto" alt="User Profile">
                    <div class="card-body">
                        <h5 class="card-title"> <i class="fa fa-user mx-1"></i> Profile Info</h5>
                        <p class="card-text"><i class="fa fa-user mx-1"></i> Eng-Nashat</p>
                        <p class="card-text"><i class="fa fa-user mx-1"></i>Admin</p>
                        <p class="card-text"><i class="fa fa-user mx-1"></i>Last Login: XxX-xXx-Xx</p>
                        <a href="#" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profile</a>
                    </div>
                </div>
            </div>

            <div class="col-md-8">

                <div class="rounded" style="width: 100%; height: 100%; background-color:whitesmoke;">
                    <h1>Welcome Admin</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            <iframe
                                src="https://free.timeanddate.com/clock/i840ds7i/n6/fn6/fs16/fcff0/tc00f/pct/ftb/bas2/bat8/bacfff/pa8/tt0/tw1/th2/ta1/tb4"
                                frameborder="0" width="217" height="62" allowtransparency="true"></iframe>

                        </div>

                        <div class="col-sm-6 ">
                            <div class="card" style="width: 20rem;">
                                <div class="card-body">
                                    <h5 class="card-title">New Orders</h5>
                                    <p class="card-text">Here you can make Invoices and create new orders.</p>
                                    <a href="#" class="btn btn-primary">New Order</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="container my-3">
        <div class="row">
            <div class="col-md-4">

                <div class="card">

                    <div class="card-body">
                        <h5 class="card-title">Categories</h5>
                        <p class="card-text">Here you Manage Categories and create new Parent and sub categories.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#form_category"
                            class="btn btn-primary">Add</a>
                        <a href="#" class="btn btn-primary">Manage</a>
                    </div>

                </div>
            </div>

            <div class="col-md-4">

                <div class="card">

                    <div class="card-body">
                        <h5 class="card-title">Brands</h5>
                        <p class="card-text">Here you Manage your Brands and create new Brands.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#form_brand" class="btn btn-primary">Add</a>
                        <a href="#" class="btn btn-primary">Manage</a>
                    </div>

                </div>

            </div>

            <div class="col-md-4">

                <div class="card">

                    <div class="card-body">
                        <h5 class="card-title">Products</h5>
                        <p class="card-text">Here you Manage your Products and create new Product.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#form_product"
                            class="btn btn-primary">Add</a>
                        <a href="#" class="btn btn-primary">Manage</a>
                    </div>

                </div>


            </div>

        </div>
    </div>

    <?php include_once './templates/category.php' ?>
    <?php include_once './templates/brand.php' ?>
    <?php include_once './templates/product.php' ?>

</body>

</html>