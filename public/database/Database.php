<?php

require_once 'config.php';

class Database
{
    private $pdoConnection;

    public function connect()
    {
        try {
            $dsn = "mysql:host=" . HOST . "; dbname=" . DB_NAME . ";charset=UTF8";
            $this->pdoConnection = new PDO($dsn, USER, PASS);
            $this->pdoConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            if ($this->pdoConnection) {
                return $this->pdoConnection;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}