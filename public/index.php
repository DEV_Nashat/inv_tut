<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <script text="text/javascript" src="js/jquery.js"></script>
    <script text="text/javascript" src="js/main.js"></script>
    <script text="text/javascript" src="js/bootstrap.bundle.js"></script>
    <script text="text/javascript" src="js/all.js"></script>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.css">

    <title>Inventory Management System</title>
</head>

<body>

    <?php

    include_once "templates/header.php";

    ?>

    <br>
    <div class="container">
        <div class="card mx-auto" style="width: 20rem;">
            <img src="./images/login.png" style="width: 60%" class="card-img-top mx-auto" alt="Login Icon">
            <div class="card-body">
                <h5 class="card-title">Log-In</h5>
                <form>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1">
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-lock">&nbsp;</i> Login
                    </button>&nbsp;
                    <span><a href="register.php">Register</a></span>
                </form>
            </div>
            <div class="card-footer">
                <a href="#">Froget Password ?!</a>
            </div>
        </div>
    </div>
</body>

</html>