<?php

class User
{

    private $connection;

    function __construct()
    {
        include_once '../database/Database.php';

        $db = new Database();

        $this->connection = $db->connect();
    }

    private function emailExists($email)
    {

        $sql = "SELECT id FROM user WHERE email = :email";

        $statement = $this->connection->prepare($sql);

        $statement->bindParam(":email", $email);

        if (!$statement->execute()) {
            echo ($this->statement->errorInfo());
            return;
        }

        $userID = $statement->fetchColumn();

        if ($userID) {

            return true;
        } else {

            return false;
        }
    }

    public function createUserAccount($userName, $email, $password, $userType)
    {
        if ($this->emailExists($email)) {

            return "Email Already Exists...";
        }

        $sql = "INSERT INTO user(username, email, password, usertype, register_date, last_login, notes) 
                VALUES(:username, :email, :password, :usertype, :register_date, :last_login, :notes)";

        $hash_pass = password_hash($password, PASSWORD_BCRYPT, ["cost" => 6]);
        $date = date('Y-m-d');
        $notes = "";

        $statement = $this->connection->prepare($sql);

        $statement->bindParam(":username", $userName);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":password", $hash_pass);
        $statement->bindParam(":usertype", $userType);
        $statement->bindParam(":register_date", $date);
        $statement->bindParam(":last_login", $date);
        $statement->bindParam(":notes", $notes);

        $result = $statement->execute();
        if ($result) {
            return $result;
        } else {
            return ($statement->errorInfo());
        }
    }
}

$user = new User();
$result = $user->createUserAccount("Wesker", "Wesker.Albert@gmail.com", "123456", "Admin");

echo $result;